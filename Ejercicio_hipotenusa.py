print("-+-+-+-+-+-ACTIVIDAD EN CLASES-+-+-+-+-+-")
print("Autora: Lilia Susana Tene.")
print("Email: lilia.tene@unl.edu.ec")
print("Fecha:Mayo 20 del 2020")

#calcular la hiputenuesa de un triangulo rectangulo dado sus catetos.
from math import sqrt
def hipot(a, b):
    hipotenusa = sqrt(a**2 + b**2)
    return hipotenusa

if __name__ == "__main__":
    a = float(input("Ingrese el cateto1: "))
    b = float(input("Ingrese el cateto2: "))
    hipot = hipot(a,b)
    print("La hipotenusa es:", hipot)
