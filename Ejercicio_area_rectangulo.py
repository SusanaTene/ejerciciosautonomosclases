print("-+-+-+-+-+-ACTIVIDAD EN CLASES-+-+-+-+-+-")
print("Autora: Lilia Susana Tene.")
print("Email: lilia.tene@unl.edu.ec")
print("Fecha:Mayo 20 del 2020")

#Crear un programa que calcule el area de un rectangulo dado

def calculo_area(base,altura):
    area = base * altura
    return area

if __name__== "__main__":
    b = float(input("Ingrese la base: "))
    a = float(input("Ingrese la altura: "))
    area = calculo_area(b, a)
    print("El are a de rectángulo es:", area)

#o  print("El are a de rectángulo es:", calculo_area(b, a))")