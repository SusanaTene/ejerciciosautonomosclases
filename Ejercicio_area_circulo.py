print("-+-+-+-+-+-ACTIVIDAD EN CLASES-+-+-+-+-+-")
print("Autora: Lilia Susana Tene.")
print("Email: lilia.tene@unl.edu.ec")
print("Fecha:Mayo 20 del 2020")

#un programa para sacar el area de un circulo

import math

def area_circulo(radio):
    area = math.pi * radio**2
    return area

if __name__== "__main__":
    r = float(input("Ingrese la radio: "))
    area = area_circulo(r)
    print("El area de circulo es:", area)